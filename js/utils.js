export function lm(type, attributes) {
    return content => {
        const element = Object.assign(document.createElement(type), attributes);
        const addContent = c => {
            if (c) {
                if (c.appendChild)
                    element.appendChild(c);
                else if (c.forEach)
                    c.forEach(addContent);
                else
                    element.innerText += c;
            }
            return element;
        }
        return addContent(content);
    }
}

export function replace_nodes(element, newNodes) {
    while (element.firstChild)
        element.removeChild(element.firstChild);
    for (let i = 0; i < newNodes.length; ++i)
        element.appendChild(newNodes[i]);
}
