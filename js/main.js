"use strict";
import '../style.scss';
import './service-worker';
import { lm, replace_nodes } from "./utils";
import { CONFIG } from "./config";
const TTG_WASM_LIB = import("../ttg-wasm-lib/target/ttg_wasm_lib");

window.addEventListener("load", () => TTG_WASM_LIB.then(init), false);

function init(wasmlib) {
    window.wasmlib = wasmlib;
    const lvl = localStorage.getItem("lvl") | 0;
    window.lvlInfos = JSON.parse(wasmlib.lvl_infos(lvl));
    window.gameData = {};
    window.addEventListener("hashchange", loadLvlFromLocationHash, false);
    window.addEventListener("click", () => gameData.win && lvlUp(), false);
    resetGameData();
    renderScoreScreen();
    if (location.hash) loadLvlFromLocationHash();
}

function loadLvlFromLocationHash() {
    if (!/^#\d+$/.test(location.hash)) return;
    let expectedResult = Math.floor(location.hash.slice(1));
    window.lvlInfos = JSON.parse(wasmlib.lvl_infos(expectedResult));
    lvlInfos.expected_result = expectedResult;
    resetGameData();
    renderGameScreen();
}

function resetGameData() {
    gameData.program = "";
    gameData.programResult = 0;
    gameData.win = false;
    gameData.vars = getVars(lvlInfos.variables_quantity);
}

function lvlUp() {
    lvlInfos = JSON.parse(wasmlib.lvl_infos(lvlInfos.lvl + 1));
    localStorage.setItem("lvl", lvlInfos.lvl);
    renderScoreScreen();
}

function getVars(n) {
    return "ABCDEF".slice(0, n).split('');
}

function getVarColor(variable) {
    const v = parseInt(variable, 36) - 9;
    const color = Array(3).fill().map((_, i) => i)
        .reduce((acc, i) => acc + (v & (1 << i) ? 8 << (i * 4) : 0), 0);
    return '#' + `000${color.toString(16)}`.slice(-3);
}

function updateProgram(newProgram) {
    programDiv.innerText = gameData.program = newProgram;
    programDiv.style.backgroundColor = CONFIG.WHITE;
    try {
        gameData.programResult = wasmlib.program_result(gameData.program, lvlInfos.variables_quantity);
    } catch (e) {
        programDiv.style.backgroundColor = CONFIG.LIGHT_RED;
        renderTruthTableBody(CONFIG.WHITE);
        return;
    }
    if (gameData.program) {
        renderTruthTableBody(CONFIG.LIGHT_GREY);

        if (gameData.programResult == lvlInfos.expected_result) {
            renderTruthTableBody(CONFIG.GREEN);
            programDiv.style.backgroundColor = CONFIG.GREEN;
            setTimeout(() => gameData.win = true, 200);
        }
    } else {
        programDiv.style.backgroundColor = CONFIG.WHITE;
        renderTruthTableBody(CONFIG.WHITE);
    }
}

function renderScoreScreen() {
    replace_nodes(document.body, [
        lm("section", {
            className: "hero is-fullheight is-dark is-bold",
            onclick: e => resetGameData() | renderGameScreen()
        })
            (lm("div", { className: "hero-body" })
                (lm("div", { className: "container has-text-centered" })
                    ([
                        lm("h1", { className: "title" })(`Truth Table Game`),
                        gameData.program ? lm("h2", { className: "subtitle" })(gameData.program) : null,
                        lm("h1", { className: "title" })(`level ${lvlInfos.lvl}`),
                        lm("h2", { className: "subtitle" })(`tap to continue`)
                    ])
                )
            )
    ]);
}

function renderGameScreen() {
    const buttonStyle = "button is-rounded is-large";
    const colWidthStyle = `width: ${Math.floor(100 / (gameData.vars.length + 2))}%`;

    replace_nodes(document.body, [
        lm("div", { className: "container" })
            ([
                lm("div", {
                    id: "programDiv",
                    className: "box has-text-centered is-size-3"
                })(gameData.program),
                lm("div", { className: "buttons box is-centered" })
                    ([
                        gameData.vars.map(lm("button", {
                            className: `${buttonStyle} is-success`,
                            onclick: onButtonClick
                        })),
                        lm("button", { className: `${buttonStyle} is-info`, onclick: onButtonClick })('~'),
                        lm("button", { className: `${buttonStyle} is-info`, onclick: onButtonClick })('&'),
                        lm("button", { className: `${buttonStyle} is-info`, onclick: onButtonClick })('|'),
                        lm("button", { className: `${buttonStyle} is-info`, onclick: onButtonClick })('^'),
                        lm("button", { className: `${buttonStyle} is-light`, onclick: onButtonClick })('←'),
                    ]),
                lm("table", {
                    className: "table is-bordered is-narrow is-hoverable is-fullwidth is-size-4"
                })
                    ([
                        lm("thead")
                            (lm("tr")
                                ([
                                    gameData.vars.map(v => lm("th", { style: `color:${getVarColor(v)}; ${colWidthStyle}` })(v)),
                                    lm("th", { style: colWidthStyle })("🎯"),
                                    lm("th", { style: colWidthStyle })("λ")
                                ])
                            ),
                        lm("tbody", { id: "truthTableBody", className: "has-text-weight-semibold" })()
                    ])
            ])
    ]);
    renderTruthTableBody(CONFIG.WHITE);
}

function renderTruthTableBody(goodResultColor) {
    const goodResultProps = { style: `background-color: ${goodResultColor}` };
    const progResAtRow = row => gameData.programResult & (1 << row);
    const expResAtRow = row => lvlInfos.expected_result & (1 << row);

    replace_nodes(truthTableBody, Array(lvlInfos.rows_quantity).fill().map((_, row) =>
        lm("tr")
            ([
                gameData.vars.map((v, col) => lm("td", { style: `color:${getVarColor(v)}` })(`${(row >> col) & 1}`)),
                lm("td", progResAtRow(row) == expResAtRow(row) ? goodResultProps : null)
                    (`${expResAtRow(row) ? 1 : 0}`),
                lm("td", progResAtRow(row) == expResAtRow(row) ? goodResultProps : null)
                    (`${progResAtRow(row) ? 1 : 0}`),
            ])
    ));
}

function onButtonClick(event) {
    const char = event.target.innerText;
    if (!gameData.win)
        updateProgram(char == '←' ? gameData.program.slice(0, -1) : gameData.program + char);
}
