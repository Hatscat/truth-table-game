#[cfg(test)]
mod tests {
    #[test]
    fn log2_tests() {
        use game_lvl::log2;
        assert_eq!(log2(0), 0);
        assert_eq!(log2(0xF), 3);
        assert_eq!(log2(0x80), 7);
    }

    #[test]
    fn one_rot_one_rot_sum_tests() {
        use game_lvl::one_rot_one_rot_sum;
        assert_eq!(one_rot_one_rot_sum(0), 0);
        assert_eq!(one_rot_one_rot_sum(2), 4);
        assert_eq!(one_rot_one_rot_sum(5), 0x10114);
    }

    #[test]
    fn shuffle_index_tests() {
        use game_lvl::shuffle_index;
        assert_eq!(shuffle_index(0, 1 << (1 << 1)), 2);
        assert_eq!(shuffle_index(3, 1 << (1 << 1)), 3);
        assert_eq!(shuffle_index(5, 1 << (1 << 2)), 7);
    }

    #[test]
    fn get_lvl_infos_tests() {
        use game_lvl::get_lvl_infos;

        let lvl_0 = get_lvl_infos(0);
        assert_eq!(lvl_0.lvl, 0);
        assert_eq!(lvl_0.variables_quantity, 1);
        assert_eq!(lvl_0.rows_quantity, 2);
        assert_eq!(lvl_0.expected_result, 2);

        let lvl_5 = get_lvl_infos(5);
        assert_eq!(lvl_5.lvl, 5);
        assert_eq!(lvl_5.variables_quantity, 2);
        assert_eq!(lvl_5.rows_quantity, 4);
        assert_eq!(lvl_5.expected_result, 4);

        let lvl_666 = get_lvl_infos(666);
        assert_eq!(lvl_666.lvl, 666);
        assert_eq!(lvl_666.variables_quantity, 4);
        assert_eq!(lvl_666.rows_quantity, 0x10);
        assert_eq!(lvl_666.expected_result, 1595);
    }
}

#[derive(Serialize, PartialEq, Debug)]
pub struct LvlInfos {
    pub lvl: u64,
    pub variables_quantity: usize,
    pub rows_quantity: usize,
    pub expected_result: u64,
}

pub fn get_lvl_infos(lvl: u64) -> LvlInfos {
    let variables_quantity =
        1 + log2(log2(lvl - one_rot_one_rot_sum(log2(log2(lvl) as u64)) as u64) as u64);
    let rows_quantity = 1 << variables_quantity;

    LvlInfos {
        lvl: lvl,
        variables_quantity: variables_quantity,
        rows_quantity: rows_quantity,
        expected_result: shuffle_index(
            lvl - one_rot_one_rot_sum(variables_quantity) as u64,
            1 << rows_quantity,
        ),
    }
}

fn log2(n: u64) -> usize {
    format!("{:b}", n).len() - 1
}

fn one_rot_one_rot_sum(n: usize) -> usize {
    (1..n).fold(0, |acc, i| acc + (1 << (1 << i)))
}

fn shuffle_index(i: u64, len: u64) -> u64 {
    let c = (len - i - 1 + 6771) % len;
    let w = 1 << 2;
    let h = len / w;
    let y = c / w;
    let x = c % w;
    x * h + y
}
