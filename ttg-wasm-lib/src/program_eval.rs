use std::char;
use std::cmp;
use std::collections::HashMap;
use std::str::Chars;

#[cfg(test)]
mod tests {
    #[test]
    fn get_variable_values_tests() {
        use program_eval::get_variable_values;
        assert_eq!(get_variable_values(1).get(&'A').unwrap(), &0b10);
        assert_eq!(get_variable_values(2).get(&'B').unwrap(), &0b1100);
        assert_eq!(get_variable_values(3).get(&'C').unwrap(), &0xF0);
        assert_eq!(get_variable_values(4).get(&'D').unwrap(), &0xFF00);
        assert_eq!(get_variable_values(5).get(&'E').unwrap(), &0xFFFF0000);
        assert_eq!(
            get_variable_values(6).get(&'F').unwrap(),
            &0xFFFFFFFF00000000
        );
        assert_eq!(
            get_variable_values(42).get(&'A').unwrap(),
            &0xAAAAAAAAAAAAAAAA
        );
    }

    #[test]
    fn get_program_result_tests() {
        use program_eval::get_program_result;
        use program_eval::EvalError;
        assert_eq!(get_program_result(None, 3), Ok(0));
        assert_eq!(get_program_result(Some("".to_string()), 2), Ok(0));
        assert_eq!(get_program_result(Some("A".to_string()), 1), Ok(2));
        assert_eq!(
            get_program_result(Some("A&".to_string()), 1),
            Err(EvalError::EmptyStack)
        );
        assert_eq!(
            get_program_result(Some("AB&".to_string()), 1),
            Err(EvalError::UnknownVariable)
        );
        assert_eq!(
            get_program_result(Some("Hello world!".to_string()), 2),
            Err(EvalError::UnknownVariable)
        );
        assert_eq!(get_program_result(Some("BA|C^".to_string()), 3), Ok(30));
        assert_eq!(get_program_result(Some("AC^".to_string()), 3), Ok(90));
        assert_eq!(
            get_program_result(Some("ABC&&AB|^".to_string()), 3),
            Ok(110)
        );
        assert_eq!(
            get_program_result(Some("EDCBA~|^&".to_string()), 5),
            Ok(754986240)
        );
        assert_eq!(
            get_program_result(Some("ABCDEF^|&~&".to_string()), 6),
            Ok(922125269585366220)
        );
    }
}

#[derive(PartialEq, Debug)]
pub enum EvalError {
    EmptyStack,
    UnknownVariable,
}

fn map_stack_value(value: Option<u64>) -> Result<u64, EvalError> {
    match value {
        Some(n) => Ok(n),
        None => Err(EvalError::EmptyStack),
    }
}

fn map_variable_value(var: Option<&u64>) -> Result<u64, EvalError> {
    match var {
        Some(n) => Ok(*n),
        None => Err(EvalError::UnknownVariable),
    }
}

pub fn get_program_result(
    program: Option<String>,
    variables_quantity: u8,
) -> Result<u64, EvalError> {
    let max_value = match variables_quantity {
        n if n < 6 => (1 << (1 << variables_quantity)) - 1,
        _ => 0xFFFFFFFFFFFFFFFF,
    };

    match program {
        None => Ok(0),
        Some(prog) => {
            if prog.is_empty() {
                Ok(0)
            } else {
                Ok(
                    eval_program(prog.chars(), get_variable_values(variables_quantity))?
                        & max_value,
                )
            }
        }
    }
}

fn get_variable_values(variables_quantity: u8) -> HashMap<char, u64> {
    let variables_quantity = cmp::min(variables_quantity, 6) as u32;

    (0..variables_quantity)
        .map(|n| {
            let mut val: u64 = 0;
            for row in 0..1 << variables_quantity {
                val += ((row >> n) & 1) << row
            }
            (
                char::from_digit(n + 10, 16).unwrap().to_ascii_uppercase(),
                val,
            )
        })
        .collect()
}

fn eval_program(
    instructions: Chars,
    variables_values: HashMap<char, u64>,
) -> Result<u64, EvalError> {
    let mut stack = Vec::<u64>::new();

    for inst in instructions {
        match inst {
            '~' => {
                let n = map_stack_value(stack.pop())?;
                stack.push(!n)
            }
            '&' => {
                let (a, b) = (map_stack_value(stack.pop())?, map_stack_value(stack.pop())?);
                stack.push(a & b);
            }
            '|' => {
                let (a, b) = (map_stack_value(stack.pop())?, map_stack_value(stack.pop())?);
                stack.push(a | b);
            }
            '^' => {
                let (a, b) = (map_stack_value(stack.pop())?, map_stack_value(stack.pop())?);
                stack.push(a ^ b);
            }
            var => {
                let n = map_variable_value(variables_values.get(&var))?;
                stack.push(n);
            }
        }
    }
    Ok(map_stack_value(stack.pop())?)
}
