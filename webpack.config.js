const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const PUBLIC_PATH = 'https://hatscat.gitlab.io/truth-table-game/';

module.exports = {
    entry: "./js/main.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "index.js",
        // publicPath: PUBLIC_PATH
        publicPath: ''
    },
    mode: "development",
    module: {
        rules: [{
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    'css-loader',
                    'sass-loader'
                ]
            })
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "Truth Table Game",
            meta: {
                charset: 'utf-8',
                viewport: 'width=device-width, initial-scale=1'
            }
        }),
        new ExtractTextPlugin('css/style.css'),
        new SWPrecacheWebpackPlugin(
            {
                cacheId: (new Date()).toISOString(),
                dontCacheBustUrlsMatching: /\.\w{8}\./,
                filename: 'service-worker.js',
                minify: true,
                navigateFallback: PUBLIC_PATH + 'index.html',
                staticFileGlobsIgnorePatterns: [/\.map$/, /manifest\.json$/]
            }
        ),
        new WebpackPwaManifest({
            name: 'Truth Table Game',
            short_name: 'TTG',
            description: 'puzzle game about logic.',
            background_color: '#FFFFFF',
            orientation: "portrait",
            display: "fullscreen",
            start_url: "/truth-table-game/",
            icons: [
                {
                    src: path.resolve('icon.png'),
                    sizes: [96, 128, 192, 256, 384, 512],
                    destination: path.join('assets', 'icons')
                }
            ]
        })
    ]
};