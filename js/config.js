export const CONFIG = Object.freeze({
    WHITE: "#FFF",
    LIGHT_GREY: "#DDD",
    LIGHT_RED: "#E77",
    GOLD: "#FD0",
    GREEN: "#2E2"
});