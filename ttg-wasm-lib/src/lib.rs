extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

extern crate wasm_bindgen;
use wasm_bindgen::prelude::*;

mod game_lvl;
mod program_eval;

#[wasm_bindgen]
pub fn lvl_infos(lvl: u32) -> String {
    let infos = game_lvl::get_lvl_infos(lvl as u64);
    serde_json::to_string(&infos).unwrap()
}

#[wasm_bindgen]
pub fn program_result(program: Option<String>, variables_quantity: u8) -> Result<u32, JsValue> {
    match program_eval::get_program_result(program, variables_quantity) {
        Ok(result) => Ok(result as u32),
        Err(eval_error) => Err(format!("{:?}", eval_error))?,
    }
}
